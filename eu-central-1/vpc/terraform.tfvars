allowed_account_id = "476323603712" # limit to Personal account

region = "eu-central-1"

vpc_name = "WordpressDemo"

cidr_block = "172.32.0.0/16" # 172.32.0.1 - 172.32.255.254

azs             = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
private_subnets = ["172.32.0.0/24", "172.32.1.0/24", "172.32.2.0/24"] # 172.32.0.1 - 172.32.2.254 (256 IP-s per subnet)
public_subnets  = ["172.32.253.0/24", "172.32.254.0/24", "172.32.255.0/24"] # 172.32.253.1 - 172.32.255.254 (256 IP-s per subnet

map_public_ip_on_launch = true

enable_dns_hostnames = true

tags = {
  Terraform   = "true"
  Environment = "demo"
}
