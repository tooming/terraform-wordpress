provider "aws" {
  allowed_account_ids = [var.allowed_account_id]
  region              = var.region
}

module "vpc" {
  source = "./../../_modules/vpc"

  name = var.vpc_name
  cidr = var.cidr_block

  azs             = var.azs
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets

  enable_dns_hostnames = var.enable_dns_hostnames

  tags = var.tags

  map_public_ip_on_launch = var.map_public_ip_on_launch
}
