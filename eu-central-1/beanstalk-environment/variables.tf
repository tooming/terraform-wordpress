variable "allowed_account_id" {}

variable "region" {
  description = "The AWS region to create things in"
  type        = string
}

variable "namespace" {
  type        = string
  description = "Namespace, which could be your organization name, e.g. 'eg' or 'cp'"
}

variable "application_name" {
  type        = string
  description = "Application name, e.g. 'app' or 'cluster'"
}

variable "stage" {
  type        = string
  description = "Stage, e.g. 'test', 'prelive' or 'live'"
  default     = ""
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "solution_stack_name" {
  type        = string
  description = "Elastic Beanstalk stack, e.g. Docker, Go, Node, Java, IIS. For more info, see https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html"
}

variable "environment_description" {
  type        = string
  default     = ""
  description = "Short description of the Environment"
}

variable "instance_type" {
  type        = string
  default     = "t2.micro"
  description = "Instances type"
}

variable "environment_type" {
  type        = string
  default     = "LoadBalanced"
  description = "Environment type, e.g. 'LoadBalanced' or 'SingleInstance'.  If setting to 'SingleInstance', `rolling_update_type` must be set to 'Time', `updating_min_in_service` must be set to 0, and `loadbalancer_subnets` will be unused (it applies to the ELB, which does not exist in SingleInstance environments)"
}

variable "loadbalancer_type" {
  type        = string
  default     = "application"
  description = "Load Balancer type, e.g. 'application' or 'classic'"
}

variable "availability_zone_selector" {
  type        = string
  default     = "Any 2"
  description = "Availability Zone selector"
}

variable "autoscale_min" {
  type        = number
  default     = 1
  description = "Minumum instances to launch"
}

variable "autoscale_max" {
  type        = number
  default     = 3
  description = "Maximum instances to launch"
}

variable "associate_public_ip_address" {
  type        = bool
  default     = false
  description = "Whether to associate public IP addresses to the instances"
}

variable "rolling_update_type" {
  type        = string
  default     = "Health"
  description = "`Health` or `Immutable` or `RollingWithAdditionalBatch`. Set it to `Immutable` to apply the configuration change to a fresh group of instances"
}

variable "deployment_policy" {
  type        = string
  default     = "Rolling"
  description = "Deployment policy"
}

variable "health_streaming_enabled" {
  type        = bool
  default     = false
  description = "For environments with enhanced health reporting enabled, whether to create a group in CloudWatch Logs for environment health and archive Elastic Beanstalk environment health data. For information about enabling enhanced health, see aws:elasticbeanstalk:healthreporting:system."
}

variable "updating_min_in_service" {
  type        = number
  default     = 1
  description = "Minimum number of instances in service during update"
}

variable "updating_max_batch" {
  type        = number
  default     = 1
  description = "Maximum number of instances to update at once"
}

variable "enable_stream_logs" {
  type        = bool
  default     = false
  description = "Whether to create groups in CloudWatch Logs for proxy and deployment logs, and stream logs from each instance in your environment"
}

variable "elb_access_logs_s3_enabled" {
  type        = bool
  default     = false
  description = "Enable load balancer access logs to S3"
}

variable "enhanced_reporting_enabled" {
  type        = bool
  default     = true
  description = "Whether to enable \"enhanced\" health reporting for this environment.  If false, \"basic\" reporting is used.  When you set this to false, you must also set `enable_managed_actions` to false"
}

variable "healthcheck_url" {
  type        = string
  default     = "/"
  description = "Application Health Check URL. Elastic Beanstalk will call this URL to check the health of the application running on EC2 instances"
}

variable "application_port" {
  type        = number
  default     = 80
  description = "Port application is listening on"
}

variable "application_protocol" {
  type        = string
  default     = "HTTP"
  description = "Application protocol (HTTP or HTTPS)"
}

variable "http_listener_enabled" {
  type        = bool
  default     = true
  description = "Enable port 80 (http)"
}

variable "instance_refresh_enabled" {
  type        = bool
  default     = true
  description = "Enable weekly instance replacement."
}

variable "loadbalancer_certificate_arn" {
  type        = string
  default     = ""
  description = "Load Balancer SSL certificate ARN. The certificate must be present in AWS Certificate Manager"
}

variable "loadbalancer_ssl_policy" {
  type        = string
  default     = ""
  description = "Specify a security policy to apply to the listener. This option is only applicable to environments with an application load balancer"
}

variable "loadbalancer_security_groups" {
  type        = list(string)
  default     = []
  description = "Load balancer security groups"
}

variable "create_dns_record" {
  type        = number
  default     = 0
  description = "Whether to create DNS record automatically"
}

variable "dns_weight" {
  type        = number
  default     = 1
  description = "Weight of Elastic Beanstalk Environment ELB DNS entry for environment subdomain in Route53"
}

variable "create_debug_user" {
  type        = number
  default     = 0
  description = "Whether to create debug user for developers. 1 - enable, 0 - disable"
}
