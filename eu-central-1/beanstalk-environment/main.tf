provider "aws" {
  allowed_account_ids = [var.allowed_account_id]
  region              = var.region
}

data "terraform_remote_state" "beanstalk_application" {
  backend = "local"
  config = {
    path = "${path.module}/../beanstalk-application/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "${path.module}/../vpc/terraform.tfstate"
  }
}

module "elastic_beanstalk_environment" {
  source                             = "./../../_modules/beanstalk/environment"
  allowed_account_id                 = var.allowed_account_id
  namespace                          = var.namespace
  name                               = join(var.delimiter, [var.application_name, var.stage])
  application_name                   = var.application_name
  stage                              = var.stage
  description                        = var.environment_description
  region                             = var.region
  availability_zone_selector         = var.availability_zone_selector
  elastic_beanstalk_application_name = data.terraform_remote_state.beanstalk_application.outputs.application_name
  create_debug_user                  = var.create_debug_user

  instance_type               = var.instance_type
  autoscale_min               = var.autoscale_min
  autoscale_max               = var.autoscale_max
  updating_min_in_service     = var.updating_min_in_service
  updating_max_batch          = var.updating_max_batch
  environment_type            = var.environment_type
  associate_public_ip_address = var.associate_public_ip_address
  rolling_update_type         = var.rolling_update_type
  deployment_policy           = var.deployment_policy
  health_streaming_enabled    = var.health_streaming_enabled
  enable_stream_logs          = var.enable_stream_logs
  elb_access_logs_s3_enabled  = var.elb_access_logs_s3_enabled

  instance_refresh_enabled = var.instance_refresh_enabled

  loadbalancer_type       = var.loadbalancer_type
  vpc_id                  = data.terraform_remote_state.vpc.outputs.vpc_id
  loadbalancer_subnets    = data.terraform_remote_state.vpc.outputs.public_subnet_ids
  application_subnets     = data.terraform_remote_state.vpc.outputs.public_subnet_ids
  allowed_security_groups = [data.terraform_remote_state.vpc.outputs.default_sg_id]
  healthcheck_url         = var.healthcheck_url
  application_port        = var.application_port
  application_protocol    = var.application_protocol
  http_listener_enabled   = var.http_listener_enabled

  // https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html
  // https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.docker
  solution_stack_name = var.solution_stack_name
}
