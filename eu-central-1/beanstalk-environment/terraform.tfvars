allowed_account_id = "476323603712" # limit to Personal account

region = "eu-central-1"

namespace = "demo"
application_name = "wordpress"
stage = "test"

instance_type = "t2.micro"
updating_min_in_service = 1
updating_max_batch = 1
availability_zone_selector = "Any"
solution_stack_name = "64bit Amazon Linux 2018.03 v2.9.6 running PHP 7.3"
environment_description = ""
environment_type = "LoadBalanced"

instance_refresh_enabled = true

loadbalancer_ssl_policy = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
loadbalancer_security_groups = []

application_port = 80
application_protocol = "HTTP"
http_listener_enabled = true # whether to listen on port 80

associate_public_ip_address = true

deployment_policy = "Rolling"
health_streaming_enabled = false
enable_stream_logs = false
elb_access_logs_s3_enabled = true
enhanced_reporting_enabled = false # ToDo: change to true
healthcheck_url = "/"

autoscale_min = 1
autoscale_max = 3
