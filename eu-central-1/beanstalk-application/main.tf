provider "aws" {
  allowed_account_ids = [var.allowed_account_id]
  region              = var.region
}

module "elastic_beanstalk_application" {
  source      = "./../../_modules/beanstalk/application"
  namespace   = var.namespace
  name        = var.application_name
  description = var.application_description
  attributes  = var.attributes
  tags        = var.tags
}
