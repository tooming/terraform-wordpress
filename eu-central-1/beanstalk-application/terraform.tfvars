allowed_account_id = "476323603712" # limit to Personal account

region = "eu-central-1"

namespace = "demo"
application_name = "wordpress"

appversion_lifecycle_max_count = 200
appversion_lifecycle_delete_source_from_s3 = false
