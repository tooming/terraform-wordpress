variable "allowed_account_id" {}

variable "region" {
  description = "The AWS region to create things in"
  type        = string
}

variable "namespace" {
  type        = string
  description = "Namespace, which could be your organization name, e.g. 'eg' or 'cp'"
}

variable "application_name" {
  type        = string
  description = "Application name, e.g. 'app' or 'cluster'"
}

variable "application_description" {
  type        = string
  default     = ""
  description = "Elastic Beanstalk Application description"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}

variable "appversion_lifecycle_max_count" {
  type        = number
  default     = 1000
  description = "The max number of application versions to keep"
}

variable "appversion_lifecycle_delete_source_from_s3" {
  type        = bool
  default     = false
  description = "Whether to delete application versions from S3 source"
}
